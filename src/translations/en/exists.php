<?php
/**
 * exists plugin for Craft CMS 3.x
 *
 * Check if a file exists
 *
 * @link      www.flowsa.com
 * @copyright Copyright (c) 2018 Richard Frank
 */

/**
 * @author    Richard Frank
 * @package   Exists
 * @since     1.0.0
 */
return [
    'exists plugin loaded' => 'exists plugin loaded',
];
