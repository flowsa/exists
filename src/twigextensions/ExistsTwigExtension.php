<?php
/**
 * exists plugin for Craft CMS 3.x
 *
 * Check if a file exists
 *
 * @link      www.flowsa.com
 * @copyright Copyright (c) 2018 Richard Frank
 */

namespace flowsa\exists\twigextensions;

use flowsa\exists\Exists;

use Craft;
use craft\elements\Asset;

/**
 * @author    Richard Frank
 * @package   Exists
 * @since     1.0.0
 */
class ExistsTwigExtension extends \Twig_Extension
{
    // Public Methods
    // =========================================================================

    /**
     * @inheritdoc
     */
    public function getName()
    {
        return 'Exists';
    }

    /**
     * @inheritdoc
     */
    public function getFilters()
    {
        return [
            new \Twig_SimpleFilter('exists', [$this, 'exists']),
        ];
    }

    /**
     * @inheritdoc
     */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('someFunction', [$this, 'someInternalFunction']),
        ];
    }

    /**
     * @param null $text
     *
     * @return string
     */
    public function exists($file)
    {

    // we are only interested in assets 

    if ($file instanceof Asset) {

      // TODO have not tested on subfolder sites or where upload folder is below root etc.

      $volumePath = rtrim($file->getVolume()->settings['path'], "/") . "/";
      $folderPath = rtrim($file->getFolder()->path, "/") . "/";
      $assetFilePath = \Yii::getAlias($volumePath) . $folderPath . $file->filename;

      if (file_exists($assetFilePath)) {
          return true;
      }

    } 
      
    return false;

    }
}
