<?php
/**
 * exists plugin for Craft CMS 3.x
 *
 * Check if a file exists
 *
 * @link      www.flowsa.com
 * @copyright Copyright (c) 2018 Richard Frank
 */

namespace flowsa\exists;

use flowsa\exists\services\ExistsService as ExistsServiceService;
use flowsa\exists\twigextensions\ExistsTwigExtension;

use Craft;
use craft\base\Plugin;
use craft\services\Plugins;
use craft\events\PluginEvent;

use yii\base\Event;

/**
 * Class Exists
 *
 * @author    Richard Frank
 * @package   Exists
 * @since     1.0.0
 *
 * @property  ExistsServiceService $existsService
 */
class Exists extends Plugin
{
    // Static Properties
    // =========================================================================

    /**
     * @var Exists
     */
    public static $plugin;

    // Public Properties
    // =========================================================================

    /**
     * @var string
     */
    public $schemaVersion = '1.0.0';

    // Public Methods
    // =========================================================================

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        self::$plugin = $this;

        Craft::$app->view->registerTwigExtension(new ExistsTwigExtension());

        Event::on(
            Plugins::class,
            Plugins::EVENT_AFTER_INSTALL_PLUGIN,
            function (PluginEvent $event) {
                if ($event->plugin === $this) {
                }
            }
        );

        Craft::info(
            Craft::t(
                'exists',
                '{name} plugin loaded',
                ['name' => $this->name]
            ),
            __METHOD__
        );
    }

    // Protected Methods
    // =========================================================================

}
