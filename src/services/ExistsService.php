<?php
/**
 * exists plugin for Craft CMS 3.x
 *
 * Check if a file exists
 *
 * @link      www.flowsa.com
 * @copyright Copyright (c) 2018 Richard Frank
 */

namespace flowsa\exists\services;

use flowsa\exists\Exists;

use Craft;
use craft\base\Component;

/**
 * @author    Richard Frank
 * @package   Exists
 * @since     1.0.0
 */
class ExistsService extends Component
{
    // Public Methods
    // =========================================================================

    /*
     * @return mixed
     */
    public function exampleService()
    {
        $result = 'something';

        return $result;
    }
}
